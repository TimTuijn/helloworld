#include <memory>
#include <iostream>

int main() {
	std::shared_ptr<int> a(new int(3));

	std::cout << *a << std::endl;

}
